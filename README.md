# DISCLAIMER!!

- **This notes are just for me**, If you are *THINKING OF* referring this notes, I highly recommend you to **MAKE YOUR OWN NOTES**. 
- **I Guarantee!!** you'll learn more by making your own NOTES.
- **Besides Someone else's notes Might Confuse you!!**

# Preparing for PNPT?

- Please Do these 3 courses ( **PEH, OSINT, EPP** ) they are core courses for PNPT exam. I'd recommend MPP too for PNPT, although not recommended by **Heath**.
- I've talked to *PNPT Holder* on official TCM Discord server in #tcm-sec-pnpt channel he said PEH is enough. *P.S He is also eJPT, OSCP, CEH holder.* More Knowledge won't hurt, Better to be safe then sorry, Do ALL Course!!!
- [Reddit post on "*Are courses offered by TCM academy enough for passing PNPT?*"](https://www.reddit.com/r/pnpt/comments/ocwp07/is_pnpt_course_enough_for_passing_the_exam/h830dil?utm_source=share&utm_medium=web2x&context=3)
- [**PNPT ROE Glimps**](https://imgur.com/a/zoVobYb)

# Comparison Between OSCP vs PNPT.

- **TRUST ME THEY BOTH ARE REALLY DIFFERENT EXAM**. 
	- One is glorified extremely expensive CTF. Way lesser implementation in real life/ day-to-day job.
		> While PNPT is real life/day-to-day Pentesting certificate. (**AD based pentest, OSINT, external compromise, internal foothold, lateral movement, domain compromise, report writing and briefing!!**)
	- Honestly I'm not against OSCP!! It's just that if I've to choose between other certs... I'd go **eJPT->PNPT->PTX** and by now if I have extra funds left in my pocket!! -> **CREST CRT**.

- If you really want to know *DIFFERENCE BETWEEN OSCP & PNPT* there are many reviews given below check it out..

1. [**Ingo Kleiber's Best Review**](https://kleiber.me/blog/2021/08/29/TCM-Security-PNPT-Certification-Seven-Days-of-Penetration-Testing/)
1. [AJ Dumanhug's Review **ROE & Glimps of Report writing**](https://atom.hackstreetboys.ph/practical-network-penetration-testing-review/)
1. [Infinity login](https://infinitelogins.com/2021/07/18/practical-network-penetration-tester-pnpt-exam-review/)
1. [TheInfoSecPhonenix Review](https://theinfosecphoenix.wordpress.com/2021/06/01/my-experience-taking-the-pnpt-practical-network-penetration-tester-certificate/)
1. [HackstoHack Mike Dame Review](https://www.youtube.com/watch?v=2jhyPg-yzzs)
	- [*Mike Dame's Comparison oscp vs pnpt, with PNPT scope*](https://imgur.com/a/zjRCJX2)
1. [Matt Schmidth's Review](https://mattschmidt.net/2021/05/04/tcm-cpeh-exam-certification-review/)

# Guide and TCM's Recommendation.

1. [PNPT Prep/Guide Unofficial](https://github.com/CyberSecurityUP/PNPT-Preparation-Guide)
1. [PNPT recommended Academic Course ](https://imgur.com/a/Wv9cZdf)

# Finally 
- [Daniel Lowrie explains PNPT **faq**](https://youtu.be/fjxHKxBba-A)

# New Updates to PNPT, So Excited!!!!

- [PNPT Exam UPDATES](https://imgur.com/a/2NxYDic)
