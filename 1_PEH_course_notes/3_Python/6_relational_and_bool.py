#!/bin/python

def nl():
	print("\n")

# Realtional and Boolean operators

greater_than = 7 > 5
less_then = 5 < 7
greater_then_equal_to = 7 >= 7
less_then_equal_to = 7 <= 7 

# AND & OR operators.
test_and = (7 > 5) and (5 < 7) 	#True
test_and2 = (7 > 5) and (5 > 7) #False
test_or = (7 > 5) or (5 < 7) #True
test_or2 = (7 > 5) or (5 > 7) #True

print(test_and, test_and2, test_or, test_or2)

nl()

# NOT operator.
test_not = not True #False
test_not2 = not False #True
print(test_not, test_not2)


