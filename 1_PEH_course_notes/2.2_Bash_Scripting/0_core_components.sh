#!/bin/bash

# There are 3 core components of bash script
# 1) Firstly, The shebang line (#!/bin/bash)
# 2) In the Middle, Commands. The more commands you know the more you more varied the scripts you can write.
# 3) Lastly, exit line (0 = successfully; 1-255 = Unsuccessfull)

echo "This is my first line"
exit 0
